﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using CoolParking.BL.Interfaces;
using System.IO;
using System.Reflection;

namespace CoolParking.BL.Services
{
	public class LogService : ILogService
	{
		private readonly string _logFilePath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";
		private readonly ILogService _logService;
		public string LogPath { get; }

		public void Write(string logInfo)
		{

		}

		public string Read()
		{
			return "";
		}

		//public LogService(string a)
		//{
		//	_logService = new LogService(_logFilePath);
		//}
	} 
}