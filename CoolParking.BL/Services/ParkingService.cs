﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace CoolParking.BL.Services
{
	public class ParkingService : Parking, IParkingService//, IDisposable
	{
		readonly ParkingService _parkingService;
		readonly TimerService _withdrawTimer;
		readonly TimerService _logTimer;
		readonly ILogService _logService;
		Parking parking;
		//Parking parking = Parking.Instance("Транс-сервис");
		public ParkingService()
		{
			Parking parking = Parking.Instance("Транс-сервис");
		}

		public ParkingService(TimerService withdrawTimer, TimerService timer, ILogService logService)
		{

		}
		public void Dispose()
		{
			parking.Dispose();
		}

		public decimal GetBalance()
		{
			return parking.Balance;
		}

		public int GetCapacity()
		{
			return parking.GetCapacity();
		}

		public int GetFreePlaces()
		{
			return parking.GetFreePlaces();
		}

		public ReadOnlyCollection<Vehicle> GetVehicles()
		{
			return 0;
		}

		public void AddVehicle(Vehicle vehicle)
		{
			try
			{
				parking.AddVehicle(vehicle);
			}
			catch (System.InvalidOperationException exception)
			{

				Console.WriteLine(exception.Message);
			}
		}

		public void RemoveVehicle(string vehicleId)
		{
			try
			{
				if (Vehicle.)
				{
					throw new Exception("Не должно быть долга на транспортном средстве");
				}
				parking.RemoveVehicle(vehicleId);

			}
			catch (Exception)
			{

				throw;
			}
		}

		public void TopUpVehicle(string vehicleId, decimal sum)
		{

		}

		//public /*TransactionInfo[]*/void GetLastParkingTransactions()
		//{
		//	//return TransactionInfo;
		//}

		public string ReadFromLog()
		{
			return "";
		}
	} 
}
