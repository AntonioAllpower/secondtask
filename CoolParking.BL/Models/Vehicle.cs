﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using System;
using System.ComponentModel.DataAnnotations;
using System.Security.Cryptography;


namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        [RegularExpression(@"^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$",
           ErrorMessage = "ID должен быть формата ХХ-YYYY-XX (где X - любая буква английского алфавита в верхнем регистре, а Y - любая цифра, например DV-2345-KJ")]
        public string Id
        {
            get; set;
        }

        public VehicleType VehicleType
        {
            get;
        }

        public decimal Balance
        {
            get; private set;
        }

        public Vehicle()
        {

        }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            this.Id = id;
            this.VehicleType = vehicleType;
            this.Balance = balance;
        }

        public void replenishBalance(decimal summa)
        {
            Balance += summa;
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            int numLetters = 2;
            int numNumbers = 4;

            char[] letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
            char[] numbers = "0123456789".ToCharArray();

            Random rand = new Random();

            string word = "";
            for (int j = 1; j <= numLetters; j++)
            {
                int letter_num = rand.Next(0, letters.Length - 1);

                word += letters[letter_num];
            }

            word += "-";

            for (int j = 1; j <= numNumbers; j++)
            {
                int letter_num = rand.Next(0, numbers.Length - 1);

                word += numbers[letter_num];
            }

            word += "-";

            for (int j = 1; j <= numLetters; j++)
            {
                int letter_num = rand.Next(0, letters.Length - 1);

                word += letters[letter_num];
            }

            return word;

        }
    }
}

