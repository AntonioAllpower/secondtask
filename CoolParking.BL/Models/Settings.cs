﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static int initialParkingBalance = 0;

        public static int parkingCapacity = 10;

        public static DateTime billingPeriod = new DateTime(0, 0).AddSeconds(5);

        public static DateTime loggingPeriod = new DateTime(0, 0).AddSeconds(60);

        public static int passengerTariffs = 2;

        public static int freightTariffs = 5;

        public static double busTariffs = 3.5;

        public static int motorcycleTariffs = 1;
    }

}