﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static Parking instance;

        public int Balance { get; set; }
        private static object syncRoot = new Object();
        List<Vehicle> vehicleList = new List<Vehicle>(Settings.parkingCapacity);
        ReadOnlyCollection<Vehicle> readOnlyVehicle =
            new ReadOnlyCollection<Vehicle>(vehicleList);

        protected Parking()
        {
        }

        public static Parking Instance(string name)
        {
            if (instance == null)
            {
                lock (syncRoot)
                {
                    if (instance == null)
                        instance = new Parking();
                }
            }
            return instance;
        }

        public void GetVehicle()
        {
            foreach (var vehicle in vehicleList)
            {
                Console.WriteLine("В паркинге: " + vehicle.Id);
            }
        }

        public void SetBalance(int balance)
        {
            Balance = balance;
        }

        public void AddVehicle(Vehicle vehicle)
        {
            vehicleList.Add(vehicle);
        }

        public void RemoveVehicle(string id)
        {
            vehicleList.Remove(new Vehicle() {Id = id});
        }

        public int GetCapacity()
        {
            return vehicleList.Count;
        }

        public int GetFreePlaces()
        {
            return vehicleList.Capacity - vehicleList.Count;
        }
    }  
}